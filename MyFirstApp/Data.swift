//
//  Data.swift
//  MyFirstApp
//
//  Created by Lola Garavagno on 26/08/2021.
//

import Foundation

let testHabits = [
       Habit(imageName: "running", name: "Running", totalTime: 178, isFavourite: false),
       Habit(imageName: "running", name: "Coding", totalTime: 734, isFavourite: true),
       Habit(imageName: "running", name: "Reading", totalTime: 62, isFavourite: false)
]
